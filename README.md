# Getting Started with Gemograph frontend 
This is a project to get a list of the most starred Github repos that were created in the last 30 days.

note: this project is the implimentation frontend-coding-challenge for gemography
https://github.com/gemography/frontend-coding-challenge


## How to use
- The node.js and React.js should be installed on your machine
- Download or clone the repository
- On terminal: Change directory to where the yarn.lock/package.json files
- on terminal : npm install
- on terminsl : yarn start
the project will run an the default browser




