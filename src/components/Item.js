import React from 'react'
import {Card, Button, Container, Row, Col,Image} from 'react-bootstrap'

export default function Item(props) {
    const {item} = props
    // console.log(item)
    return (
        <Card className='mb-3 center' style={{ width: '45rem' }}>
          <Card.Header>{item.full_name}</Card.Header>
            <Container>
              <Row>
                <Col md={3}>
                  <Image  src={item.owner.avatar_url} width='150' height='150' roundedCircle alt='no avatar available'/>
                </Col>
                <Col md={8}>
                  <Card.Body>
                    {/* <Card.Title>{item.full_name}</Card.Title> */}
                    <Card.Text style={{color:item.description?'black':'red'}}>
                      {item.description?item.description:'no description available'}
                    </Card.Text>
                    <Button variant="outline-primary" size="sm">Stars: {item.watchers}</Button>{' '}
                    <Button variant="outline-warning" size="sm">Issues: {item.open_issues}</Button>{' '}
                    <i>Submitted {Math.round((new Date()-new Date(item.pushed_at))/(1000*60*60*24))} day ago by {item.owner.login}</i> 
                   
                  </Card.Body>
                </Col>
              </Row>
            </Container>
        </Card> 
    )
}
