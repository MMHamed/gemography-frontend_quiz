import React, { Component } from 'react'


import Item from './Item'

export class App extends Component {
  state={
    newData:[],
    page:1,
    loading:false,
    prevY:0,
    listEnd:false
  }

  handleObsersver(entities, observer){
    const y =entities[0].boundingClientRect.y
    if (this.state.prevY > y) {
      this.setState({ page: this.state.page+1 });
      this.getRepo();

    }

    this.setState({ prevY: y });

  }

  getRepo(){
    this.setState({loading:true})
    const month = new Date().getMonth()
    const date = new Date().toISOString().slice(0,10).replace((month+1).toString(),month.toString())
    
    fetch(`https://api.github.com/search/repositories?q=created:>${date}&sort=stars&order=desc&page=${this.state.page}&per_page=10`)
   .then(response =>response.json())
   .then(data=>{
     if (data.items !== undefined ){
       this.setState({newData: this.state.newData.length ===0? [...data.items]:[...this.state.newData, ...data.items]})
      this.setState({loading:false})
     }else{this.setState({loading:false, listEnd:true})}
     
    })
   .catch(error => {
    console.error('Catch Error:', error)
    this.setState({loading:false, listEnd:true})
  })
  }
  componentDidMount(){
    this.getRepo()
    this.observer = new IntersectionObserver(
    this.handleObsersver.bind(this),
    {
      root:null,
      rootMargin:'0px',
      threshold:1.0
    }
  )
  this.observer.observe(this.loadingRef);
  }
  
  componentWillUnmount() {
    // fix Warning: Can't perform a React state update on an unmounted component
    this.setState = (state,callback)=>{
        return;
    };
}

  render() {
    const {newData,loading, listEnd} = this.state
    console.log(this.state)

    // Additional css
    const loadingCSS = {
      height: "100px",
      margin: "30px"
    };


    // To change the loading icon behavior
    const loadingTextCSS = { display: loading ? "block" : "none" };
    const endListTextCSS = {display: listEnd ? "block" : "none"}

      return (
        <div>
          <ul>
          {newData.length !== 0 &&
            newData.map((item,index)=>
              <li key={index}>
                <Item  item ={item}/>
              </li>
            )
          } 
          </ul>
          <div
              ref={loadingRef => (this.loadingRef = loadingRef)}
              style={loadingCSS}
              >
              <span style={loadingTextCSS}>Loading...</span>
              <span style={endListTextCSS}>This is the end of the repository list</span>
          </div>  
          
        </div>
      )
  }
}

export default App



